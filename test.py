import numpy as np
import pandas as pd
from functions import *

dataframe=pd.read_csv('out.csv')
print(" mean " , mean(dataframe['col']))
print("median ", median(dataframe['col']))
print("standardDeviation " , standardDeviation(dataframe['col']))
print("Moving Average " ,movingAverage(dataframe['col'],4))
print("Moving Variance ", movingVar(dataframe['col'],4))
print("Using pandas mean: ", dataframe.col.mean())
print("Using pandas median ", dataframe.col.median())
print("Using pandas mode :" , dataframe.col.mode())
print("Using pandas stddev : ",dataframe.col.values.std(ddof=0))
print("Using pandas percentile: " , dataframe.col.quantile(0.68))
print("Using pandas moving average: \n",dataframe.rolling(2).mean())
print("Using pandas moving stddev: \n",dataframe.rolling(2).std())