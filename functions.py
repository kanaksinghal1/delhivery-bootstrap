#Mean
def mean(l):
    s=0
    count=0
    for i in l:
        count+=1
        s=s+int(i)
    
    m=s/count
    return m

 #Median
def median(list):
    leng=len(list)
    mid=(int)(leng/2)
    if leng==1:
        return list[0]
    
    elif leng%2 == 1:
        mid=(int)(leng/2)
        return list[mid]
    else:
        middle=list[mid-1]+list[mid]
        middle=middle/2
        return middle
        
#Standard Deviation
def standardDeviation(list):
    meani=mean(list)
    leng=len(list)
    sum=0
    for i in list:
        sum+=abs(i-meani)**2
    sum=sum/leng
    sum=sum**(0.5)
    return sum

 #Moving Average
def movingAverage(list,sizeA):
    leng=len(list)
    movingAverageL = []
  
    if sizeA <= leng :
        sum=0
        for i in range(0,sizeA):
            sum+=list[i]
       # print("sizeA " , sizeA)    
        ans=sum/sizeA   
        movingAverageL.append(ans)
        #print(" size %d    leng = %d  " %(sizeA,leng))
        for j in range(sizeA,leng):
            ans = ans*sizeA + list[j] - list[j-sizeA]
            #print("j =  %d  sum = %d  list[j] = %d  list[j-sizeA] = %d "   %(j,ans,list[j],list[j-sizeA]))
            ans=ans/sizeA
            movingAverageL.append(ans)
    return movingAverageL

 #Moving Variance
def movingVar(list,sizeV):
    leng=len(list)
    movingVar=[]
    if sizeV <= leng:
        variance = 0
        for i in range(0,leng-sizeV+1):
            meani=mean(list[i:i+sizeV])
            for j in range(i,i+sizeV):
                variance+=(list[j]-meani)**2
            variance=variance/sizeV
            movingVar.append(variance)
        
        
        return movingVar
    