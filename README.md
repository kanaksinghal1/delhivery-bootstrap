#out.csv
	
	This is a csv file which contains 25 random values.
#functions.py
	
	This python file contain different functions such as
	Mean
	Median
	Standard Deviation
	Moving Average
	Moving variance

#test.py
	
	Tested all functions of functions.py the data is imported from out.csv and all the functions implemented in functions.py is again tested using pandas library. 
	
#ToPostgres.py
	
	Connected python3 to postgres using psycopg2,to apply functions such as mean to data fetched from tables in postgres.
